const gameManagement = require('../../hooks/game-management');
const questionHook = require('../../hooks/question-hook');
const {authenticate} = require('@feathersjs/authentication').hooks;

const dateAppend = require('../../hooks/date-append');

module.exports = {
  //TODO: find a better war for getting scheduled game
  before: {
    all: [],
    find: [],
    get: [authenticate('jwt')],
    create: [authenticate('jwt')],
    update: [authenticate('jwt')],
    patch: [authenticate('jwt')],
    remove: [authenticate('jwt')]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [gameManagement()],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
