// Initializes the `game` service on path `/game`

const numeral = require('numeral');
const axios = require('axios');
const createService = require('feathers-mongodb');
const hooks = require('./game.hooks');
const customMethod = require('feathers-custom-methods');
let correctAnswer;
let question_id;
let users = [];
let answerA = 0;
let answerB = 0;
let answerC = 0;
let total = 0;
let winners = [];

let response = {
  question_id: "12000000",
  choices: [
    {identifier: "A", count: answerA},
    {identifier: "B", count: answerB},
    {identifier: "C", count: answerC},
  ],
  total: total
};

module.exports = function (app) {
  const paginate = app.get('paginate');
  const mongoClient = app.get('mongoClient');

  let events = ['gamefinished', 'usersAnswer', "startIntro"];

  const options = {events, paginate};

  // Initialize our service with any options it requires
  app.use('/game', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('game');

  mongoClient.then(db => {
    service.Model = db.collection('game');
  });

  service.hooks(hooks);

  app.service('question').on('updated', function (response) {
    correctAnswer = response.correctAnswer
  });

  service.on('next_question', () => {
    resetCount();
    setTimeout(() => {
      service.emit('usersAnswer', response);
    }, 12000)
  });

  service.on('gamefinishedlocal', (game_status) => {
    //console.log(WinnersList(game_status.prize, users));

    let wonAmount = (game_status.prize / users.length);

    users = users.filter(user => {
      return user.startsWith("+251")
        || user.startsWith("251")
        || user.startsWith("09")
        || user.startsWith("00251")
    });

    axios({
      'url': `http://178.62.81.205:3000/accounts/payout`,
      'data': {
        "title": "Game 27",
        "amount": wonAmount,
        "accounts": users
      },
      'headers': {
        "Authorization": "Basic bWVkYTp0M0BtTTNEQDIwMTghISE=",
        "Content-Type": "application/json",
        "d83a1331fe0d678055081903eb32eb4a": "EACA2F7F034E5B6B4E0933F422FC31B3",
        "f0197f80c13b2a1f33ed39e14982b701": "B0173F568680817D04CC725304DA9F9C",
        "f918602fded3acfaec0f1a8495ee45c2": "C131123A5BA32411D2FFC734587B5250",
        "d437c734e49e06e82ad7c9f086c1d923": "EF6F06E8ED962DEBF221F81CBB3309C4",
      },
      'method': "POST",
      'withCredentials': true
    }).then(function (response) {
      console.log("Payment made for the winners")
    }).catch(function (error) {
      console.log(error);
    });

    app.service('users').patch(null, {'$inc': {won: wonAmount}}, {
      query: {
        phone_number: {
          $in: users
        }
      }
    });

    service.emit('gamefinished', {
      'game_id': game_status.id,
      'finished': true,
      'totalWinners': users.length,
      'each': numeral(game_status.prize / users.length).format('0,0'),
      'totalPrice': game_status.prize
    });
    resetCount()
  });

  service.answer = function (data) {
    const selected = data.answer;
    ++total;
    if (selected === correctAnswer) {
      users.push(data.user);
    }
    if (question_id !== data.question_id) {
      question_id = data.question_id;
    }
    if (selected === "A") {
      ++answerA;
    }
    else if (selected === "B") {
      ++answerB;
    }
    else if (selected === "C") {
      ++answerC;
    }
    response = {
      question_id: question_id,
      choices: [
        {identifier: "A", count: answerA},
        {identifier: "B", count: answerB},
        {identifier: "C", count: answerC},
      ],
      total: total
    }
  };
};

function resetCount() {
  //answerA = answerB = answerC = total = 0;
  users = [];
  answerA = 0;
  answerB = 0;
  answerC = 0;
  total = 0;
}
