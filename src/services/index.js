const question = require('./question/question.service.js');
const tags = require('./tags/tags.service.js');
const game = require('./game/game.service.js');
const users = require('./users/users.service.js');
const category = require('./category/category.service.js');
const messages = require('./messages/messages.service.js');
const advertisement = require('./advertisement/advertisement.service.js');
const news = require('./news/news.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(question);
  app.configure(tags);
  app.configure(game);
  app.configure(users);
  app.configure(category);
  app.configure(messages);
  app.configure(advertisement);
  app.configure(news);
};
