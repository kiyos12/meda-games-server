const {authenticate} = require('@feathersjs/authentication').hooks;
const userAvatar = require('../../hooks/user-avatar');
const userAnswer = require('../../hooks/users-find-create');
const {hashPassword, protect} = require('@feathersjs/authentication-local').hooks;

const lifeUpdate = require('../../hooks/life-update');

module.exports = {
  before: {
    all: [],
    find: [authenticate('jwt')],
    get: [authenticate('jwt')],
    create: [hashPassword(), userAvatar(), userAnswer()],
    update: [hashPassword(), authenticate('jwt'), lifeUpdate()],
    patch: [],
    remove: []
  },

  after: {
    all: [
      // Make sure the password field is never sent to the client
      // Always must be the last hook
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
