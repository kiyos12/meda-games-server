// Initializes the `Advertisement` service on path `/advertisement`
const createService = require('feathers-mongodb');
const hooks = require('./advertisement.hooks');
const UtillMethods = require("../../utills/UtillMethods");

module.exports = function (app) {
  const paginate = app.get('paginate');
  const mongoClient = app.get('mongoClient');
  const options = { paginate };

  // Initialize our service with any options it requires
  app.use('/advertisement', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('advertisement');

  mongoClient.then(db => {
    service.Model = db.collection('advertisement');
  });

  service.hooks(hooks);
};
