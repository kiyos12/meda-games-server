// Initializes the `category` service on path `/category`
const createService = require('feathers-mongodb');
const hooks = require('./category.hooks');

module.exports = function (app) {
  const paginate = app.get('paginate');
  const mongoClient = app.get('mongoClient');
  const options = { paginate };

  // Initialize our service with any options it requires
  app.use('/category', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('category');

  mongoClient.then(db => {
    service.Model = db.collection('category');
  });

  service.hooks(hooks);
};
