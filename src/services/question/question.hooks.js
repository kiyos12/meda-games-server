const {authenticate} = require('@feathersjs/authentication').hooks;
const search = require('feathers-mongodb-fuzzy-search');
const question_hook = require('../../hooks/question-hook');

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [question_hook()],
    patch: [],
    remove: []
  }
};
