// Initializes the `question` service on path `/question`
const createService = require('feathers-mongodb');
const hooks = require('./question.hooks');

module.exports = function (app) {
  const paginate = app.get('paginate');
  const mongoClient = app.get('mongoClient');
  const options = {paginate};

  // Initialize our service with any options it requires
  app.use('/question', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('question');

  mongoClient.then(db => {
    service.Model = db.collection('question');
    service.Model.createIndex({ question: 'text' });
  });

  service.hooks(hooks);
};
