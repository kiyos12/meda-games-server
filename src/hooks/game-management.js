// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
// eslint-disable-next-line no-unused-vars
const UtillMethods = require("../utills/UtillMethods");
const Poll = require("../utills/poll");
const Agenda = require("agenda");
const kue = require('kue-scheduler');

const connectionString = "mongodb://127.0.0.1/game_schedules";

const agenda = new Agenda({db: {address: connectionString, collection: 'jobs'}});

module.exports = function (options = {}) {

  function onSchedule(context) {

    let isCalled = false;

    context.app.services.game.on('updated', (response) => {

      if (response.scheduled === "true" && !isCalled) {

        isCalled = true;

        const gameSignature = new Date().getTime();

        const game_time = new Date(response.start_time);

        const game_time_minus_5 = new Date(new Date(response.start_time).getTime() - 305000);

        console.log(game_time_minus_5.toString());

        console.log(game_time.toString());

        agenda.schedule(game_time_minus_5, 'schedule_game_intro_' + gameSignature);

        agenda.schedule(game_time, 'schedule_game_' + gameSignature);

        agenda.define('schedule_game_' + gameSignature, function (job, done) {
          setGameStarted(context, response).then((response) => {
            loopQuestions(context, response.questions, response._id);
            done();
          })
        });

        agenda.define('schedule_game_intro_' + gameSignature, function (job, done) {
          context.app.services.messages.delete(null);
          UtillMethods.sendNotification(UtillMethods.generateMessage("ጨዋታው መጀመሩ ነው፣ ወደ ሜዳ ሺ አፑን እንክፈተው"));
          console.log("Start game intro");
          context.app.services.game.emit('startIntro', response);
          done();
        });

        agenda.start();

        agenda.on('error', function (error) {
          console.log("Agenda Error " + error);
        });
      }
    })
  }

  function setGameStarted(context, response) {
    return context.app.services.game.update(response._id, {
      $set: {
        scheduled: "false",
        started: "true"
      }
    });
  }

  function setGameFinished(context, id) {
    return context.app.services.game.update(id, {
      $set: {
        started: "false",
        finished: "true"
      }
    });
  }

  function loopQuestions(context, response, id) {
    let i = 0;
    console.log(Poll.version);
    Poll.start({
      name: "update_users",
      start: 500,
      interval: 28000,
      action: function () {
        if (i === response.length) {
          setGameFinished(context, id).then((response) => {
            context.app.services.game.emit('gamefinishedlocal', {
              game_id: id,
              finished: true,
              prize: response.prize,
            });
          });
          return false;
        }
        if (i === response.length - 1) {
          console.log("Last Question");
        }
        context.app.services.game.emit('next_question', true);
        context.app.services.question.update(response[i].id, {
          $set: {
            asked_games: id,
            server_time: new Date().getTime()
          }
        });
        ++i;
      },
    });
  }

  return async context => {
    onSchedule(context);
    return context;
  };
};
