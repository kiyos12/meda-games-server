// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
const Emoji = require('../utills/Constants')
// eslint-disable-next-line no-unused-vars
module.exports = function (options = {}) {
  return async context => {
    let index = Math.floor(Math.random() * 978);
    context.data.avatar_emoji = Emoji[index];
    return context;
  };
};
