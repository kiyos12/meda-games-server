// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = function (options = {}) {

  return async context => {
    /*context.data.createdAt = new Date();
    if (context.method === 'update') {
      context.data.updateAt = new Date();
    } else if (context.method === 'create') {
      context.data.createdAt = new Date();
    }*/
    //context.service.create({question:'Sample Question',text: 'text1'});

    //console.log(context.app.services.game);

    /*context.app.services.question.find().then(response => {
      loopQuestions(context, response.data)
    });

    let users = ["5b0fa9a2d45f0b1ee4739199", "5b0fa9a2d45f0b1ee4739199"];

    context.service.update("5b13ba5a725ead1d10103f8e", {winners: users}).then(res => console.log(res));
  */

    await context.app.services.question.update(context.id, {
      $set: {
        server_time: new Date().getTime()
      }
    });

    return context;
  };


};
