const errors = require('@feathersjs/errors');
// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = function (options = {}) {
  return async context => {
    if (typeof context.params.user.referred === 'undefined') {
      await context.app.services.users.patch(null, {$set: {referred: true}}, {
        query: {
          _id: context.params.user._id
        }
      });
    }
    else {
      context.result = new errors.BadRequest('You already referred');
    }
    return context;
  };
};
