// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = function (options = {}) {
  return async context => {
    const service = context.service;

    return service.find({query: {game: context.data.game}})
      .then(async response => {

        response = response.data;
        if (response.length) {
          // Set `hook.result` to skip the db call to `create` if a record was found.
          await context.app.service('news').update(response[0]._id, context.data);

          context.result = response[0];
        }
        // Return the `hook` object for the next hook to use.
        return context;
      });
  };
};
