// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
// eslint-disable-next-line no-unused-vars
'use strict';
/**
 * If the record doesn't already exist, create it and return it to the user.
 */
module.exports = function (options = {}) {
  return async hook => {
    const service = hook.service;

    return service.find({query: {meda_id: hook.data.meda_id}})
      .then(response => {

        response = response.data;
        if (response.length) {
          // Set `hook.result` to skip the db call to `create` if a record was found.
          hook.result = response[0];
        }
        // Return the `hook` object for the next hook to use.
        return hook;
      });
  }
};
