module.exports = function (options = {}) {
  return function headers(req, res, next) {
    if (req.headers.from === "1c231214-37d8-11e8-b467-0ed5f89f718b") {
      next();
    } else {
      res.send({
        status: "Error",
        description: "you are not accessing from Meda app"
      })
    }
  };
};
