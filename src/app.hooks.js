// Application hooks that run for every service
const logger = require('./hooks/logger');
const search = require('feathers-mongodb-fuzzy-search');

module.exports = {
  before: {
    all: [ logger() ],
    find: [search({ escape: false })],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [ logger() ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [ logger() ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
