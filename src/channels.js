module.exports = function (app) {
  if (typeof app.channel !== 'function') {
    // If no real-time functionality has been configured just return
    return;
  }

  const channelAnonymous = app.channel('anonymous');

  const channelAuthenticated = app.channel('authenticated');

  app.on('connection', connection => {
    //On a new real-time connection, add it to the anonymous channel
    channelAnonymous.join(connection);

    console.log("Connected "+channelAnonymous.length);
  });

 /* app.on('disconnection', connection => {
    //On a new real-time connection, add it to the anonymous channel
    channelAnonymous.join(connection);

    //console.log(channelAnonymous.length);

    app.service('users').emit('usersonline', {
      type: 'usersonline',
      count: channelAnonymous.length
    })
  });*/

  app.on('disconnect', disconnected => {
    app.service('users').emit('usersonline', {
      type: 'usersonline',
      count: channelAuthenticated.length
    })
  });

  app.on('login', (authResult, {connection}) => {
    // connection can be undefined if there is no
    // real-time connection, e.g. when logging in via REST
    if (connection) {
      // Obtain the logged in user from the connection
      // const user = connection.user;

      // The connection is no longer anonymous, remove it
      channelAnonymous.leave(connection);

      // Add it to the authenticated user channel
      console.log(connection.user.meda_name);

      channelAuthenticated.join(connection);

      console.log("Authenticated "+channelAuthenticated.length);

      app.service('users').emit('usersonline', {
        type: 'usersonline',
        count: channelAuthenticated.length
      })

      // Channels can be named anything and joined on any condition

      // E.g. to send real-time events only to admins use
      // if(user.isAdmin) { app.channel('admins').join(connection); }

      // If the user has joined e.g. chat rooms
      // if(Array.isArray(user.rooms)) user.rooms.forEach(room => app.channel(`rooms/${room.id}`).join(channel));

      // Easily organize users by email and userid for things like messaging
      // app.channel(`emails/${user.email}`).join(channel);
      // app.channel(`userIds/$(user.id}`).join(channel);
    }
  });

  app.publish('patched', (data, hook) => {

    console.log('Publishing all events to all authenticated users. See `channels.js` and https://docs.feathersjs.com/api/channels.html for more information.'); // eslint-disable-line

    return app.channel('authenticated');
  });

  app.service('question').publish('updated', (data) => {
    return channelAuthenticated;
  });

  app.service('users').publish('usersonline', (data) => {
    return app.channel('authenticated');
  });

  app.service('game').publish('gamefinished', (data) => {
    console.log("Game Finished " + JSON.stringify(data));
    return app.channel('authenticated');
  });

  app.service('game').publish('usersAnswer', (data) => {
    console.log("Users Answer " + JSON.stringify(data));
    return app.channel('authenticated');
  });

  app.service('game').publish('startIntro', (data) => {
    console.log("Start Intro " + data);
    return app.channel('authenticated');
  });

  /*app.service('game').publish('updated', (data, hook) => {
    console.log(data.started);
    if(data.started){
      console.log('Publishing update event for game '+data._id); // eslint-disable-line
      return app.channel('anonymous');
    }
  });*/

  // eslint-disable-next-line no-unused-vars
  app.service('messages').publish('created', (data, hook) => {
    // Here you can add event publishers to channels set up in `channels.js`
    // To publish only for a specific event use `app.publish(eventname, () => {})`

    console.log('Publishing all events to all authenticated users. See `channels.js` and https://docs.feathersjs.com/api/channels.html for more information.'); // eslint-disable-line

    // e.g. to publish all service events to all authenticated users use
    return app.channel('authenticated');
  });

  // eslint-disable-next-line no-unused-vars
  app.service('news').publish('created', (data, hook) => {
   return app.channel('authenticated');
  });

  // eslint-disable-next-line no-unused-vars
  app.service('news').publish('updated', (data, hook) => {
    return app.channel('authenticated');
  });

  // Here you can also add service specific event publishers
  // e..g the publish the `users` service `created` event to the `admins` channel
  // app.service('users').publish('created', () => app.channel('admins'));

  // With the userid and email organization from above you can easily select involved users
  // app.service('messages').publish(() => {
  //   return [
  //     app.channel(`userIds/${data.createdBy}`),
  //     app.channel(`emails/${data.recipientEmail}`)
  //   ];
  // });
};
