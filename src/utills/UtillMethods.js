const FCM = require('fcm-node');

module.exports = {
  subtractMinutesFromDate: (date, minute) => {
    const temp = new Date(date);
    temp.setMinutes(date.getMinutes() - minute);
    return temp;
  },

  generateMessage: (message) => {
    return {
      "to": "/topics/meda",
      "data": {
        "title": "ሜዳ ሺ",
        "body": message,
        "sound": "default",
        "click_action": "{\"type\":\"activity\", \"action\":\"ShiActivity\"}",
      },
      "ttl": 3600,
      "icon": "ic_app_notify",
      "time_to_live": 300
    };
  },

  sendNotification: (message) => {
    const SERVER_KEY = "AIzaSyAdyujsmNxXHe5XiJVSSJ024-9Jkit-tfY";
    const fcm = new FCM(SERVER_KEY);

    fcm.send(message, (err, response) => {
      console.log(err, response);
      if (err) {
        console.log("Something has gone wrong! " + err);
      } else {
        console.log("Successfully sent with response: ", response);
      }
    })
  }
};
