const assert = require('assert');
const app = require('../../src/app');

describe('\'Advertisement\' service', () => {
  it('registered the service', () => {
    const service = app.service('advertisement');

    assert.ok(service, 'Registered the service');
  });
});
