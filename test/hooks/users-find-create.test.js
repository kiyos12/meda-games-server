const assert = require('assert');
const feathers = require('@feathersjs/feathers');
const usersAnswer = require('../../src/hooks/users-find-create');

describe('\'users-answer\' hook', () => {
  let app;

  beforeEach(() => {
    app = feathers();

    app.use('/dummy', {
      async get(id) {
        return { id };
      }
    });

    app.service('dummy').hooks({
      before: usersAnswer()
    });
  });

  it('runs the hook', async () => {
    const result = await app.service('dummy').get('test');

    assert.deepEqual(result, { id: 'test' });
  });
});
